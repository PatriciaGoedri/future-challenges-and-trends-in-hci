using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using PDollarGestureRecognizer;
using System.Collections.Specialized;
using JetBrains.Annotations;
using System.IO;

public class Movement : MonoBehaviour
{
    private bool isMoving = false;
    public GameObject rightController;

    public GameObject debugCube;

    public float newPosListDist = 0.05f;

    public bool creationMode = true;
    public string gestureName;

    public GameObject cubePrefab;

    public GameObject trianglePrefab;

    public GameObject circlePrefab;

    private List<Gesture> trainingSet = new List<Gesture>();

    private List<Vector3> positionsList = new List<Vector3>();
    // Start is called before the first frame update
    void Start()
    {
        string[] gestureFiles = Directory.GetFiles(Application.persistentDataPath, "*.xml");
        
        foreach (var item in gestureFiles)
        {
            trainingSet.Add(GestureIO.ReadGestureFromFile(item));
        }
    }

    // Update is called once per frame
    void Update()
    {



        if (!isMoving && OVRInput.Get(OVRInput.Button.One))
        {

            StartMovement();

        }

        else if (isMoving && !(OVRInput.Get(OVRInput.Button.One)))
        {
            EndMovement();
        }
        else if (isMoving && OVRInput.Get(OVRInput.Button.One))

        {
            UpdateMovement();

        }
        
    }

    public void StartMovement()
    {
        isMoving = true;

        positionsList.Clear();

        positionsList.Add(rightController.transform.position);
        if(debugCube)
            Destroy(Instantiate(debugCube, rightController.transform.position, Quaternion.identity ),3);
    }

    public void EndMovement()
    {
        isMoving = false;

        //create gesture
        Point[] pointArray = new Point[positionsList.Count];

        for (int i = 0; i < positionsList.Count; i++)
        {
           Vector2 screenPoint = Camera.main.WorldToScreenPoint(positionsList[i]);
           pointArray[i] = new Point(screenPoint.x, screenPoint.y,0);
        }

        Gesture newGesture = new Gesture(pointArray);

        if(creationMode)
        {
            newGesture.Name = gestureName;
            trainingSet.Add(newGesture);

            string fileName = Application.persistentDataPath + "/" + gestureName + ".xml";
            GestureIO.WriteGesture(pointArray, gestureName, fileName);

            Debug.Log("Gesture Storeeeeeed");

            Debug.Log(Application.persistentDataPath + "/" + gestureName + ".xml");

        }

        else 
        {
            Result result = PointCloudRecognizer.Classify(newGesture, trainingSet.ToArray());

            Debug.Log("shooooooooooooooow meeeeeeee");

            Debug.Log(result.GestureClass + result.Score);

            if (result.GestureClass == "Cube" && result.Score > 0.85)
            {
                Debug.Log("CREATING CUBE");
                Instantiate(cubePrefab, rightController.transform.position, Quaternion.identity);

            }
            else if (result.GestureClass == "Cone" && result.Score > 0.85)
            {
                Debug.Log("CREATING TRIANGLE");
                Instantiate(trianglePrefab, rightController.transform.position, Quaternion.identity);
                
            }
            else if (result.GestureClass == "Circle" && result.Score > 0.85)
            {
                Debug.Log("CREATING CIRCLE" );
                Instantiate(circlePrefab, rightController.transform.position, Quaternion.identity);
                
            }
        }



    }

    public void UpdateMovement()
    {
        Vector3 lastPos = positionsList[positionsList.Count -1];
        if(Vector3.Distance(rightController.transform.position, lastPos) > newPosListDist)
        {
            positionsList.Add(rightController.transform.position);
            if(debugCube)
            Destroy(Instantiate(debugCube, rightController.transform.position, Quaternion.identity ),3);

        }
            
        
    }
}
