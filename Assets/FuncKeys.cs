using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuncKeys : MonoBehaviour
{
    public GameObject cubePrefab;

    public GameObject circlePrefab;

    public GameObject trianglePrefab;
    public GameObject cameraRig;
    public GameObject rightControllerAnchor;

    public bool existing = true;

 


    // Update is called once per frame
    void Update()
    {

        // Check if right trigger button was pressed this frame
        if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
        {
            // If button was pressed, instantiate cube and the controller position
            Instantiate(cubePrefab, rightControllerAnchor.transform.position, Quaternion.identity);

            //OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)
        }

        // Check if right trigger button was pressed this frame
        if (OVRInput.Get(OVRInput.Button.One))
        {
            if(existing)
            {
                existing = false;
            // If button was pressed, instantiate cube and the controller position
            Instantiate(circlePrefab, rightControllerAnchor.transform.position, Quaternion.identity);

            }

            //OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)
        }

        if (!(OVRInput.Get(OVRInput.Button.One)) && existing == false)
        {
            existing = true;
        }

        // Check if right trigger button was pressed this frame
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            // If button was pressed, instantiate cube and the controller position
            Instantiate(trianglePrefab, rightControllerAnchor.transform.position, Quaternion.identity);

            //OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch)
        }

        
    }
}
