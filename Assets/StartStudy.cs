using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class StartStudy : MonoBehaviour
{

    public GameObject study;
    public GameObject myPos;
    public Vector3 position1;

    public GameObject ConeStudy;

    public GameObject CubeStudy;

    public GameObject SphereStudy;


    // Start is called before the first frame update
    void Start()
    {

      
        
    }

    // Update is called once per frame
    void Update()
    {

        if (OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.8f) 
        {
            Instantiate(CubeStudy, new Vector3(-2f, .2f, -6.75f ), Quaternion.identity);

            Instantiate(ConeStudy, new Vector3(-.5f, .6f, -6f ), Quaternion.identity);

            Instantiate(SphereStudy, new Vector3(-1f, .3f, -7f ), Quaternion.identity);

        }
        
    }
}
