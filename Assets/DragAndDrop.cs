using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SubsystemsImplementation;

public class DragAndDrop : MonoBehaviour
{

    public GameObject cubePrefab;

    public GameObject circlePrefab;

    public GameObject trianglePrefab;
    public GameObject cameraRig;
    public GameObject rightControllerAnchor;
    public GameObject newObject;

    GameObject touchedObject;

    GameObject grabbedObject;

    Vector3 cubePositionOrigin;
    Vector3 trianglePositionOrigin;
    Vector3 circlePositionOrigin;

    public bool cubeAccessed = false;

    
    public void Start(){

        cubePositionOrigin = cubePrefab.transform.position;

        trianglePositionOrigin = trianglePrefab.transform.position;

        circlePositionOrigin = circlePrefab.transform.position;



    }


   public void OnTriggerEnter(Collider other){


       if (other.gameObject.name == "Cube" ) 
       {
           Debug.Log("WHY SO STUPID CUBE");

            


    
            GameObject clone = Instantiate(cubePrefab, cubePrefab.transform.position , Quaternion.identity);

            Debug.Log("ICH HEISSE:" + clone.name);

                 

            
                
            
        }

        if (other.gameObject.name == "Cube(Clone)" )
            {
                if (!grabbedObject)
                {
                touchedObject = other.gameObject;
            }

       }




       if (other.gameObject.name == "Cone" ) 
       {
           Debug.Log("CONE");

            GameObject clone = Instantiate(trianglePrefab, trianglePrefab.transform.position , Quaternion.identity);

            Debug.Log("ICH HEISSE:" + clone.name);

            
        }

        if (other.gameObject.name == "Cone(Clone)" )
            {

                
                if (!grabbedObject)
                {
                touchedObject = other.gameObject;
        }

       }



       if (other.gameObject.name == "Sphere" ) 
       {
           Debug.Log("Sphere");

            GameObject clone = Instantiate(circlePrefab, circlePositionOrigin , Quaternion.identity);

            Debug.Log("ICH HEISSE:" + clone.name);

            
        }

        if (other.gameObject.name == "Sphere(Clone)" )
            {

                
                if (!grabbedObject)
                {
                touchedObject = other.gameObject;
        }

       }

       
       
   }

   public void OnTriggerExit(Collider other)
    {
        touchedObject = null;
    }


    // Update is called once per frame
    void Update()
    {

        if (OVRInput.Get(OVRInput.Button.One) && touchedObject)
        {
           
            grabbedObject = touchedObject;
            grabbedObject.transform.SetParent(this.transform);
          
        }

        if ( !(OVRInput.Get(OVRInput.Button.One)) && grabbedObject)
        {
            grabbedObject.transform.SetParent(null);
            grabbedObject = null;
        }


        
    }
}
