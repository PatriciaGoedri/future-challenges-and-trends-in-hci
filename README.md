# Standardized Tasks

Standardized tasks are an important aspect of human computer interaction.
Using these, several interaction techniques can be empirically studied or compared and discussed with previous research. This could help to create a universal testing ground for further research.

This project provides a reimplementation of a standardized task for selection/positioning by W. Buxton from 1983.
Using this task W. Buxton compares five interaction technique in 2D space using a monitor, a keyboard and a graphical mouse.

1. Drag and Drop
2. Moving Menu
3. Character recognition
4. Typing
5. Function keys

Within this reimplementation the Oculus Quest 2 will be used. All interaction except the typing will be reimplemented.

**Drag and Drop**

**Moving Menu**

**Character Recognition**

**Function keys**

